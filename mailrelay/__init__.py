"""An app for relaying Eve mails to Discord."""

# pylint: disable=invalid-name
default_app_config = "mailrelay.apps.MailrelayConfig"

__version__ = "1.4.0"
__title__ = "Mail Relay"
